console.log("=== Function ==")
console.log("=== Function Soal No 1 ==")
function teriak() {
    return "Halo Sanbers!"
}
console.log(teriak())

console.log("=== Function Soal No 2 ==")
function kalikan(x, y) {
    return x * y
}
var num1 = 12
var num2 = 4
var hasilperkalian = kalikan(num1, num2)
console.log(hasilperkalian);


console.log("=== Function Soal No 3 ==")
/* 
    Tulis kode function di sini
*/
function introduce(name, age, address, hobby) {
    var kalimat = "Nama saya adalah " + " " + name + " " + "Usia saya" + " " + age + " " + "alamat saya " + " " + address + " " + "Hobby Saya" + " " + hobby + "!"
    return kalimat
}

var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!" 