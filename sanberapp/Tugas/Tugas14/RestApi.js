import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import { StyleSheet, Text, View, TextInput, Button, TouchableOpacity } from 'react-native'

const Item = ({ title, value, onPress, onDelete }) => {
    return (
        <View style={styles.itemContainer}>
            <View style={styles.news}>
                <TouchableOpacity onPress={onPress}>
                    <Text style={styles.newsTitle}>{title}</Text>

                </TouchableOpacity>
                <Text style={styles.newsValue}>{value}</Text>
            </View>
            <TouchableOpacity onPress={onDelete}>
                <Text style={styles.Delete}>X</Text>
            </TouchableOpacity>
        </View>
    )
}
const RestApi = () => {

    const [title, setTitle] = useState("");
    const [value, setValue] = useState("");
    const [news, setNews] = useState([]);
    const [button, setButton] = useState("Simpan");
    const [selectedUser, setSelectedUser] = useState([])

    useEffect(() => {
        getDataNews()
    }, [])
    const submit = () => {
        const data = {
            title, value
        }
        if (button === 'Simpan') {

            Axios.post('https://achmadhilmy-sanbercode.my.id/api/v1/news', data)
                .then(res => {
                    console.log('res:', res);
                    setTitle("");
                    setValue("");
                    getDataNews();
                })
        } else if (button === 'Update') {
            Axios.put(`https://achmadhilmy-sanbercode.my.id/api/v1/news/${selectedUser.id}`, data)
                .then(res => {
                    console.log('res Update:', res);
                    setTitle("");
                    setValue("");
                    getDataNews();
                    setButton("Simpan");

                })
        }
    }
    const getDataNews = () => {
        Axios.get('https://achmadhilmy-sanbercode.my.id/api/v1/news')
            .then((res) => {
                setNews(res.data.data)
            })
            .catch((err) => {
                alert(err)
            })
    }
    const selectItem = (item) => {
        console.log('Selected item:', item);
        setSelectedUser(item);
        setTitle(item.title);
        setValue(item.value);
        setButton("Update");
    }
    const deleteItem = (item) => {
        Axios.delete(`https://achmadhilmy-sanbercode.my.id/api/v1/news/${item.id}`)
            .then(res => {
                console.log('res: ', res)
                getDataNews()
            }).catch(err => {
                console.log('error: ', err)
            })
    }
    return (
        <View style={styles.container}>
            <Text style={styles.TextTitle}>Local API JSON</Text>
            <Text>Masukan Data News</Text>

            <TextInput placeholder="title" style={styles.input} value={title} onChangeText={(value) => setTitle(value)} />

            <TextInput placeholder="value" style={styles.input} value={value} onChangeText={(value) => setValue(value)} />
            <Button title={button} onPress={submit} />
            <View style={styles.Line} />
            {
                news.map((item) => {
                    return <Item key={item.id} title={item.title} value={item.value} onPress={() => selectItem(item)} onDelete={() => deleteItem(item)} />
                })
            }


        </View>

    )
}
export default RestApi

const styles = StyleSheet.create({
    container: {
        padding: 20
    },
    TextTitle: {
        textAlign: 'center',
        marginBottom: 20
    },
    Line: {
        height: 2, backgroundColor: 'black', marginVertical: 20
    },
    avatar: {
        width: 100, height: 100, borderRadius: 100
    },
    input: {
        borderWidth: 1, marginBottom: 12,
        borderRadius: 25, paddingHorizontal: 12
    },
    itemContainer: {
        flexDirection: 'row', marginBottom: 20
    },
    news: {
        marginLeft: 18, flex: 1
    },
    newsTitle: {
        fontSize: 20,
        fontWeight: 'bold'
    },
    newsValue: {
        fontSize: 16
    },
    Delete: {
        fontSize: 20, fontWeight: "bold", color: 'red'
    }


})