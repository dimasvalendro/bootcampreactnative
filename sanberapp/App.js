import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
// import Login from './Tugas/Tugas13/LoginScreen';
// import About from './Tugas/Tugas13/AboutScreen';
// import RestApi from './Tugas/Tugas14/RestApi';
import Tugas15 from './Tugas/Tugas15/index';
// import Index from './Tugas/Quiz3/index';


export default function App() {
  return (
    // <RestApi />
    <Tugas15 />
    // <Index />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
