console.log("== Soal No 1 ==")
function range(startNum, finishNum) {
    var rangeArr = [];

    if (startNum > finishNum) {
        var rangeLength = startNum - finishNum + 1;
        for (var i = 0; i < rangeLength; i++) {
            rangeArr.push(startNum - i)
        }
    } else if (startNum < finishNum) {
        var rangeLength = finishNum - startNum + 1;
        for (var i = 0; i < rangeLength; i++) {
            rangeArr.push(startNum + i)
        }
    } else if (!startNum || !finishNum) {
        return -1
    }
    return rangeArr

}
console.log("=== Hasil ====")
console.log(range(1, 10))
console.log(range(11, 18))
console.log(range(54, 50))
console.log(range(1))
console.log(range())

console.log("== Soal No 2 ==")
function rangeWithStep(startNum, finishNum, step) {
    var rangeArr = [];

    if (startNum > finishNum) {
        var currentNum = startNum;
        for (var i = 0; currentNum >= finishNum; i++) {
            rangeArr.push(currentNum)
            currentNum -= step
        }
    } else if (startNum < finishNum) {
        var currentNum = startNum;
        for (var i = 0; currentNum <= finishNum; i++) {
            rangeArr.push(currentNum)
            currentNum += step
        }
    } else if (!startNum || !finishNum || step) {
        return -1
    }
    return rangeArr

}
console.log("== hasil no 2 ==")
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 


console.log("== Soal No 3 ==")
function sum(startNum, finishNum, step) {
    var rangeArr = [];
    var distance;

    if (!step) {
        distance = 1
    } else {
        distance = step
    }
    if (startNum > finishNum) {
        var currentNum = startNum;
        for (var i = 0; currentNum >= finishNum; i++) {
            rangeArr.push(currentNum)
            currentNum -= distance
        }
    } else if (startNum < finishNum) {
        var currentNum = startNum;
        for (var i = 0; currentNum <= finishNum; i++) {
            rangeArr.push(currentNum)
            currentNum += distance
        }
    } else if (!startNum && !finishNum && step) {
        return 0
    } else if (startNum) {
        return startNum
    }
    var total = 0;
    for (var i = 0; i < rangeArr.length; i++) {
        total = total + rangeArr[i]
    }
    return total

}
// Code di sini
console.log("== hasil no 3 ==")
console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0

console.log("== Soal No 4 ==")
//contoh input
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
function dataHandling(data) {
    var dataLength = data.length
    for (i = 0; i < dataLength; i++) {
        var id = "no id : " + data[i][0]
        var nama = "nama lengkap : " + data[i][1]
        var ttl = "TTL : " + data[i][2] + " " + data[i][3]
        var hobi = "Hobi : " + data[i][4]
        console.log(id)
        console.log(nama)
        console.log(ttl)
        console.log(hobi)
    }
}
console.log("== hasil no 4 ==")
dataHandling(input)

console.log("== Soal No 5 ==")
function balikKata(kata) {
    var kata1 = " ";
    for (var i = kata.length - 1; i >= 0; i--) {
        kata1 += kata[i]
    }
    return kata1;
}
console.log("==Hasil no 5===")
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

console.log("== Soal No 6 ==")
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
function dataHandling2(data) {
    var newdata = data
    var newnama = data[1] + "Elsharawy"
    var newprovince = data[2] + "propinsi"
    var gender = "pria"
    var institusi = "SMA Internasional Metro"

    newdata.splice(1, 1, newnama)
    newdata.splice(2, 1, newprovince)
    newdata.splice(4, 1, institusi)

    var arrDate = data[3]
    var newDate = arrDate.split('/')
    var monthNum = newDate[1]
    var monthnama = " "

    switch (monthNum) {
        case "01":
            monthnama = "Januari"
            break;
        case "02":
            monthnama = "Februari"
            break;
        case "03":
            monthnama = "Maret"
            break;
        case "04":
            monthnama = "April"
            break;
        case "05":
            monthnama = "Mei"
            break;
        case "06":
            monthnama = "Juni"
            break;
        case "07":
            monthnama = "Juli"
            break;
        case "08":
            monthnama = "Agustus"
            break;
        case "09":
            monthnama = "September"
            break;
        case "10":
            monthnama = "Oktober"
            break;
        case "11":
            monthnama = "November"
            break;
        case "12":
            monthnama = "Desember"
            break;
        default:
            break;
    }
    var datejoin = newDate.join("-")
    var dateArr = newDate.sort(function (value1, value2) {
        value2 - value1
    })
    var editNama = newnama.slice(0, 15)
    console.log(newdata)
    console.log(monthnama)
    console.log(dateArr)
    console.log(datejoin)
    console.log(editNama)
}

/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */