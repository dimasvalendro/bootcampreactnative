function arrayToObject(arr) {
    if (arr.length <= 0) {
        return console.log("")
    }
    for (var i = 0; i < arr.length; i++) {
        var newObject = {}
        var birthYear = arr[i][3];
        var now = new Date().getFullYear()
        var newAge;
        if (birthYear && now - birthYear > 0) {
            newAge = now - birthYear
        } else {
            newAge = "Invalid Birth Year"
        }
        newObject.firstname = arr[i][0]
        newObject.lastname = arr[i][1]
        newObject.gender = arr[i][2]
        newObject.age = newAge

        var consoleText = (i + 1) + '-' + newObject.firstname + ' ' + newObject.lastname + ' : '
        console.log(consoleText)
        console.log(newObject)
    }
}
console.log("== Soal No 1 ==")
// Driver Code
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)
var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2)



function shoppingTime(memberId, money) {
    if (!memberId) {
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    } else if (money < 50000) {
        return "Mohon maaf, uang tidak cukup"
    } else {
        var newObject = {}
        var moneyChange = money;
        var purchaselist = [];
        var sepatuStacattu = "sepatu stacattu";
        var bajuZoro = "baju Zoro";
        var bajuHn = "Baju H & N"
        var sweaterUniklooh = "Sweater Uniklooh";
        var casingHandphone = "casing Handphone";

        var check = 0;
        for (var i = 0; moneyChange >= 50000 && check == 0; i++) {
            if (moneyChange >= 150000) {
                purchaselist.push(sepatuStacattu)
                moneyChange -= 150000
            } else if (moneyChange >= 50000) {
                purchaselist.push(bajuZoro)
                moneyChange -= 50000
            }
            else if (moneyChange >= 250000) {
                purchaselist.push(bajuHn)
                moneyChange -= 250000
            } else if (moneyChange >= 175000) {
                purchaselist.push(sweaterUniklooh)
            } else if (moneyChange >= 50000) {
                for (var j = 0; j <= purchaselist.length - 1; j++) {
                    if (purchaselist[j] == casingHandphone) {
                        check += 1
                    }
                    if (check == 0) {
                        purchaselist.push(casingHandphone)
                        moneyChange -= 50000
                    } else {
                        purchaselist.push(casingHandphone)
                        moneyChange -= 50000
                    }
                }
            }
            newObject.memberId = memberId
            newObject.money = money
            newObject.listPurchased = purchaselist
            newObject.changeMoney = moneyChange
            return newObject
        }
    }
}
console.log("== Soal No 2==")
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

function naikAngkot(arrPenumpang) {
    var rute = ["A", "B", "C", "D", "E", "F"]
    var arrOutput = []
    if (arrPenumpang.length <= 0) {
        return []
    }
    for (var i = 0; i < arrPenumpang.length; i++) {
        var objOutput = {}
        var asal = arrPenumpang[i][1]
        var tujuan = arrPenumpang[i][2]
        var IndexAsal;
        var IndexTujuan;

        for (var j = 0; j < rute.length; j++) {
            if (rute[j] == asal) {
                IndexAsal = j
            } else if (rute[j] == tujuan) {
                IndexTujuan = j
            }
        }
        var bayar = (IndexTujuan - IndexAsal) * 2000
        objOutput.penumpang = arrPenumpang[i][0]
        objOutput.naikDari = asal
        objOutput.tujuan = tujuan
        objOutput.bayar = bayar

        arrOutput.push(objOutput)
    }
    return arrOutput
}

console.log("== Soal No 3 ==")
console.log(naikAngkot([["Dimitri", "B", "F"], ["Icha", "A", "B"]]));
console.log(naikAngkot([])); //[]