console.log("==SOAL NO 1==")
console.log("Merubah Function ke Arrow Function")
const golden = () => {
    console.log("this is golden!!")
}

golden();
console.log("==SOAL NO 2==")
console.log("Object Literal")
const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName: () => {
            console.log(firstName + " " + lastName)
            return
        }
    }
}

//Driver Code 
newFunction("William", "Imoh").fullName()

console.log("==SOAL NO 3==")
console.log("Destructuring")
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
const { firstName, lastName, destination, occupation, spell } = newObject
console.log(firstName, lastName, destination, occupation, spell)

console.log("==SOAL NO 4 ==")
console.log("Array Spreading")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
console.log(combined)

console.log("==SOAL NO 4 ==")
console.log("Template Literals")
const planet = "earth"
const view = "glass"
var before = `Lorem  ${view} dolor sit amet,
    consectetur adipiscing elit, ${planet} do eiusmod tempor
    incididunt ut labore et dolore magna aliqua. Ut enim
    ad minim veniam`

// Driver Code
console.log(before) 