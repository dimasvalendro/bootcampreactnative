import React, { useState, useEffect } from 'react'
import { Button, Alert, Platform, StyleSheet, Text, View, TouchableOpacity, Image, KeyboardAvoidingView, ScrollView, TextInput, } from 'react-native'
import * as firebase from 'firebase'

export default function index({ navigation }) {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    // Your web app's Firebase configuration
    var firebaseConfig = {
        apiKey: "AIzaSyCyVZyuI4atKFbk-6rwrIOEUakoa36aNLY",
        authDomain: "finalprojectrnsanber.firebaseapp.com",
        projectId: "finalprojectrnsanber",
        storageBucket: "finalprojectrnsanber.appspot.com",
        messagingSenderId: "555575132556",
        appId: "1:555575132556:web:5233c721632ff312e316a9"
    };
    // Initialize Firebase
    if (!firebase.apps.length) {
        firebase.initializeApp(firebaseConfig);

    }
    const login = () => {
        const data = {
            email, password
        }
        if (email == '' || password == '') {
            Alert.alert('Error', 'Form tidak boleh kosong')
        } else {
            firebase.auth().signInWithEmailAndPassword(email, password)
                .then((res) => {
                    navigation.navigate('MyDrawer', {
                        screen: 'Beranda',
                        params: {
                            screen: 'About'
                        }
                    })
                })
                .catch((err) => {
                    Alert.alert('Error', err.message)
                })
        }
    }
    return (
        <KeyboardAvoidingView
            behavior={Platform.OS == "ios" ? "padding" : "height"}
            style={styles.container}>
            <ScrollView>
                <View>
                    <Image style={styles.logo} source={require('../../asset/logo.png')} />
                    <Text style={styles.LoginText}>LOGIN</Text>
                    <View style={styles.FormInput}>
                        <Text style={styles.FormText}>Username</Text>
                        <TextInput
                            value={email}
                            onChangeText={(value) => setEmail(value)}
                            style={styles.Input} />
                    </View>
                    <View style={styles.FormInput}>
                        <Text style={styles.FormText}>Password</Text>
                        <TextInput
                            value={password}
                            onChangeText={(value) => setPassword(value)}
                            style={styles.Input} secureTextEntry={true} />
                    </View>
                </View>
                <View style={styles.kotaklogin}>
                    <TouchableOpacity style={styles.ButtonLogin}
                        onPress={(login)}
                    // onPress={() => navigation.navigate("MyDrawer", {
                    //     screen: 'App',
                    //     params: {
                    //         screen: 'About'
                    //     }
                    // })}
                    >
                        <Text style={styles.TextButton}>SIGN IN</Text>
                    </TouchableOpacity>
                    <Text style={styles.autotext}>Atau</Text>
                    <TouchableOpacity style={styles.ButtonRegister}
                        onPress={() => navigation.navigate("Register")}
                    >
                        <Text style={styles.TextButton}>Registrasi</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </KeyboardAvoidingView>


        // <View style={styles.container}>
        //     <Text>Login</Text>

        // <Button
        //     borderradius="10"
        //     color="#841584"
        // onPress={() => navigation.navigate("MyDrawer", {
        //     screen: 'App',
        //     params: {
        //         screen: 'About'
        //     }
        // })}
        //     title="Login" />
        //     <Text>Atau</Text>
        //     <Button onPress={() => navigation.navigate("Register")} title="Register" />
        // </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginVertical: 30
    },

    logo: {
        width: 168,
        height: 168,
        alignSelf: 'center',
        marginTop: 30,

    },
    LoginText: {
        fontSize: 36,
        padding: 18,
        textAlign: 'center',
        color: '#000000',
        marginBottom: 39

    },
    FormText: {
        color: '#000000',
        fontWeight: 'bold'

    },
    autotext: {
        fontSize: 30,
        color: '#000000',
        textAlign: 'center',
        margin: 5,
    },
    FormInput: {
        marginHorizontal: 50,
        marginVertical: 6,
        alignContent: 'center',
        width: 294,
        margin: 25,

    },
    Input: {
        height: 50,
        borderColor: '#000000',
        borderWidth: 2,
        borderRadius: 10,

    },
    ButtonLogin: {
        alignItems: 'center',
        backgroundColor: '#000000',
        padding: 7,
        borderRadius: 10,
        marginHorizontal: 25,
        marginVertical: 10,
        marginBottom: 5,
        width: 140,
        height: 40


    },
    ButtonRegister: {
        alignItems: 'center',
        backgroundColor: '#000000',
        padding: 7,
        borderRadius: 10,
        marginHorizontal: 25,
        marginVertical: 10,
        width: 140,
        height: 40

    },
    kotaklogin: {
        alignItems: 'center',

    },
    TextButton: {
        color: 'white'
    }

})
