import React, { useEffect, useState, Component } from 'react'
import { Button, StyleSheet, ScrollView, SafeAreaView, FlatList, Text, View, Image, ImageBackground, TouchableOpacity, Alert } from 'react-native'
import * as firebase from 'firebase';
import { HeaderStyleInterpolators } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

export default function index({ navigation }) {

    const [user, setUser] = useState({})

    useEffect(() => {
        const userInfo = firebase.auth().currentUser
        setUser(userInfo)
    }, [])

    const Logout = () => {
        firebase.auth().signOut()
            .then(() => {
                Alert.alert('user berhasil logout')
                navigation.navigate("Login")
            })
    }


    //     var firebaseConfig = {
    //         apiKey: "AIzaSyCyVZyuI4atKFbk-6rwrIOEUakoa36aNLY",
    //         authDomain: "finalprojectrnsanber.firebaseapp.com",
    //         projectId: "finalprojectrnsanber",
    //         storageBucket: "finalprojectrnsanber.appspot.com",
    //         messagingSenderId: "555575132556",
    //         appId: "1:555575132556:web:5233c721632ff312e316a9"
    //     };
    //     // Initialize Firebase
    //     if (!firebase.apps.length) {
    //         firebase.initializeApp(firebaseConfig);

    //     }

    //     const [data, setData] = useState({})
    //     console.log(data)

    //     const getData = () => {

    //         const myinfomasjid = firebase.database().ref("infomasjid/")
    //         myinfomasjid.on("value", snapshot => {
    //             const data = snapshot.val()
    //             setData(data)
    //         })
    //     }
    //     useEffect(() => {
    //         getData();
    //     }, [])
    // const array = []
    // data.keys(data).forEach((key) => {
    //     array.push({ [key]: data[key] })
    // })
    // array.push(data)
    // for (let x = 0; x < array.length; x++) {
    //     console.log(array[x].alamat)
    // }
    return (
        // <View style={styles.container}>
        //     <Text>data</Text>

        // </View>
        // <FlatList
        //     data={items}
        //     keyExtractor={(item, index) => `${item.key}-${index}`}
        //     renderItem={({ item }) => {
        //         <View>
        //             <Text>{item.name}</Text>
        //         </View>
        //     }}
        // />

        <SafeAreaView>
            <ScrollView>
                <View style={styles.container}>

                    <View style={styles.banner}>
                        <ImageBackground
                            style={styles.ImageBackground} source={require('../../asset/banner.jpg')}
                        >
                            <Text style={styles.textimageback}>Selamat Datang</Text>
                            <Text style={styles.textimageback}>Perhimpunan Remaja Masjid </Text>
                            <Text style={styles.textimageback}>Dewan Masjid Indonesia </Text>
                            <Text style={styles.textimageback}>Jakarta Utara </Text>

                        </ImageBackground>
                        {/* <ImageBackground style={styles.ImageBackground} source={require('../../asset/banner.png')} /> */}

                    </View>
                    <View>

                        <Text style={styles.user}>Hello, {user.email}</Text>
                        <TouchableOpacity style={styles.btnLogout} >
                            <Text style={styles.btnLogoutText} onPress={Logout}>Logout</Text>
                        </TouchableOpacity>
                    </View>
                    <Text style={styles.juduldalam}>Info Prima DMI JAKARTA UTARA</Text>
                    <View style={styles.kotak}>
                        <View style={styles.kotakdalam}>
                            <Image
                                style={{
                                    height: 100,
                                    width: 150,
                                    marginLeft: 25,
                                    alignSelf: 'center'
                                }}
                                source={require('../../asset/masjid1.png')} />
                            <View style={styles.textnama}>
                                <Text style={styles.textcontent}>Masjid Islamic Center</Text>
                                <Text style={styles.textcontent}> Jl. Kramat Jaya Raya,
                                RW.1, Tugu Utara, Kec. Koja,
                                Kota Jkt Utara,
                    Daerah Khusus Ibukota Jakarta 14260</Text>
                            </View>



                        </View>
                        <View style={styles.kotakdalam}>
                            <Image
                                style={{
                                    height: 100,
                                    width: 150,
                                    marginLeft: 25,
                                    alignSelf: 'center'
                                }}
                                source={require('../../asset/masjid1.png')} />
                            <View style={styles.textnama}>
                                <Text style={styles.textcontent}>Masjid Islamic Center</Text>
                                <Text style={styles.textcontent}> Jl. Kramat Jaya Raya,
                                RW.1, Tugu Utara, Kec. Koja,
                                Kota Jkt Utara,
                    Daerah Khusus Ibukota Jakarta 14260</Text>
                            </View>
                        </View>
                        <View style={styles.kotakdalam}>
                            <Image
                                style={{
                                    height: 100,
                                    width: 150,
                                    marginLeft: 25,
                                    alignSelf: 'center'
                                }}
                                source={require('../../asset/masjid1.png')} />
                            <View style={styles.textnama}>
                                <Text style={styles.textcontent}>Masjid Islamic Center</Text>
                                <Text style={styles.textcontent}> Jl. Kramat Jaya Raya,
                                RW.1, Tugu Utara, Kec. Koja,
                                Kota Jkt Utara,
                    Daerah Khusus Ibukota Jakarta 14260</Text>
                            </View>
                        </View>
                        <View style={styles.kotakdalam}>
                            <Image
                                style={{
                                    height: 100,
                                    width: 150,
                                    marginLeft: 25,
                                    alignSelf: 'center'
                                }}
                                source={require('../../asset/masjid1.png')} />
                            <View style={styles.textnama}>
                                <Text style={styles.textcontent}>Masjid Islamic Center</Text>
                                <Text style={styles.textcontent}> Jl. Kramat Jaya Raya,
                                RW.1, Tugu Utara, Kec. Koja,
                                Kota Jkt Utara,
                    Daerah Khusus Ibukota Jakarta 14260</Text>
                            </View>
                        </View>


                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        // flex: 1,
        // backgroundColor: '#fff',
        // alignItems: 'center',
        // justifyContent: 'center',
    },
    // item: {
    //     backgroundColor: '#f9c2ff',
    //     padding: 5,
    //     flex: 1,
    //     marginVertical: 60

    // },
    textimageback: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
    },
    ImageBackground: {
        flex: 1,
        width: 400,
        height: 250,
        alignItems: 'center',
        paddingLeft: 120,
        padding: 5,
        marginTop: 35
    },
    banner: {
        flex: 1,
    },
    kotak: {
        flex: 2,
        margin: 10,
        borderColor: 'blue',
        borderRadius: 10,
        borderBottomColor: '#000',
        backgroundColor: '#EFEFEF',
        marginBottom: 9,
    },
    kotakdalam: {
        borderWidth: 1,
        borderColor: 'black',
        borderRadius: 10,
        borderTopColor: '#003366',
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginBottom: 9,
    },
    textcontent: {
        fontSize: 16,
        width: 200,
        fontWeight: 'bold',
        color: '#003366',
        textAlign: "center"
    },

    textnama: {
        justifyContent: "center",
        padding: 10,
        marginHorizontal: 10,
        marginVertical: 10,
        marginLeft: 15
    },
    juduldalam: {
        fontSize: 18,
        color: "#003366",
        padding: 5,
        textAlign: 'center',
        marginBottom: 5,
        marginTop: 10

    },
    user: {
        fontSize: 18,
        color: "#003366",
        padding: 5,
        textAlign: 'left',
        marginBottom: 5,
        marginTop: 10

    },
    btnLogout: {
        height: 30,
        width: 100,
        borderRadius: 5,
        marginVertical: 5,
        marginLeft: 20,
        alignItems: 'center',
        backgroundColor: '#3a86ff',

    },
    btnLogoutText: {
        fontSize: 18,
        textAlign: 'center',
        fontWeight: 'bold',
        color: '#ffffff'
    }


})
