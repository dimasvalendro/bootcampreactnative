import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Alert } from 'react-native'
import { TextInput } from 'react-native-gesture-handler'
import * as firebase from 'firebase'
export default function index({ navigation }) {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    // Your web app's Firebase configuration
    var firebaseConfig = {
        apiKey: "AIzaSyCyVZyuI4atKFbk-6rwrIOEUakoa36aNLY",
        authDomain: "finalprojectrnsanber.firebaseapp.com",
        projectId: "finalprojectrnsanber",
        storageBucket: "finalprojectrnsanber.appspot.com",
        messagingSenderId: "555575132556",
        appId: "1:555575132556:web:5233c721632ff312e316a9"
    };
    // Initialize Firebase
    if (!firebase.apps.length) {
        firebase.initializeApp(firebaseConfig);

    }
    const submit = () => {
        const data = {
            email,
            password
        }
        if (email == '' || password == '') {
            Alert.alert('Error', 'Form tidak boleh kosong')
        } else {
            firebase.auth().createUserWithEmailAndPassword(email, password)
                .then((res) => {
                    Alert.alert('Sukses', 'Buat akun berhasil')
                    navigation.navigate("Login")
                })
                .catch((err) => {
                    Alert.alert('Error', err.message)
                })
        }
    }
    return (
        <View style={styles.container}>
            <Text style={styles.text}>Register</Text>
            <View>
                <View style={styles.FormInput}>
                    <Text style={styles.FormText}>Email</Text>
                    <TextInput
                        style={styles.Input}
                        placeholder="Masukin Email"
                        value={email}
                        onChangeText={(value) => setEmail(value)}
                    />
                </View>
                <View style={styles.FormInput}>
                    <Text style={styles.FormText}>Password</Text>
                    <TextInput
                        value={password}
                        onChangeText={(value) => setPassword(value)}
                        placeholder="Masukin Password"
                        style={styles.Input} secureTextEntry={true} />
                </View>
                <View style={styles.kotakregister}>
                    <TouchableOpacity style={styles.ButtonRegister}
                        onPress={submit}
                    >
                        <Text style={styles.TextButton}>Registrasi</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    text: {
        fontSize: 20,
        textAlign: 'center',
        padding: 20
    },
    FormInput: {
        marginHorizontal: 50,
        marginVertical: 6,
        alignContent: 'center',
        width: 294,
        margin: 25,
    },
    Input: {
        height: 50,
        borderColor: '#000000',
        borderWidth: 2,
        borderRadius: 10,

    },
    FormText: {
        color: '#000000',
        fontWeight: 'bold'

    },
    ButtonRegister: {
        alignItems: 'center',
        backgroundColor: '#000000',
        padding: 7,
        borderRadius: 10,
        marginHorizontal: 25,
        marginVertical: 10,
        width: 140,
        height: 40

    },
    kotakregister: {
        alignItems: 'center',

    },
    TextButton: {
        color: 'white'
    }
})
