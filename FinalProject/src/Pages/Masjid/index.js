import React, { useEffect, useState, Component } from 'react'
import { Button, StyleSheet, ScrollView, SafeAreaView, FlatList, Text, View, Image, ImageBackground, TouchableOpacity } from 'react-native'
import * as firebase from 'firebase';
import { useObjectVal } from 'react-firebase-hooks/database';

export default function index({ navigation }) {
    const [items, setItems] = useState([])
    const [user, setUser] = useState({})

    useEffect(() => {
        const userInfo = firebase.auth().currentUser
        setUser(userInfo)

    }, [])

    const namaTabel = 'infomasjid'
    const [snapshots, loading, error] = useObjectVal(firebase.database().ref(namaTabel))
    var firebaseConfig = {
        apiKey: "AIzaSyCyVZyuI4atKFbk-6rwrIOEUakoa36aNLY",
        authDomain: "finalprojectrnsanber.firebaseapp.com",
        projectId: "finalprojectrnsanber",
        storageBucket: "finalprojectrnsanber.appspot.com",
        messagingSenderId: "555575132556",
        appId: "1:555575132556:web:5233c721632ff312e316a9"
    };
    // Initialize Firebase
    if (!firebase.apps.length) {
        firebase.initializeApp(firebaseConfig);

    }
    const ambilData = () => {
        if (!loading && snapshots) {
            const arr = Object.keys(snapshots).map((k) => snapshots[k])
            setItems(arr)

        }
    }


    //     console.log(data)

    //     const getData = () => {

    //         const myinfomasjid = firebase.database().ref("infomasjid/")
    //         myinfomasjid.on("value", snapshot => {
    //             const data = snapshot.val()
    //             setData(data)
    //         })
    //     }
    //     useEffect(() => {
    //         getData();
    //     }, [])
    // const array = []
    // data.keys(data).forEach((key) => {
    //     array.push({ [key]: data[key] })
    // })
    // array.push(data)
    // for (let x = 0; x < array.length; x++) {
    //     console.log(array[x].alamat)
    // }
    return (

        // <View style={styles.container}>
        //     <Text>data</Text>

        // </View>
        <View style={styles.container}>
            <View style={styles.kotakButton}>
                <Button title="tambah masjid" onPress={() => navigation.navigate("AddInfomasjid")} />
            </View>
            <View style={styles.kotakButton2}>

                <Button title="lihat" onPress={() => ambilData()} />
            </View>
            <FlatList

                data={items}
                keyExtractor={(item) => `${item.name}`}
                renderItem={({ item }) => {
                    return (
                        <>
                            <View style={styles.itemContainer}>
                                <Image
                                    style={{
                                        height: 100,
                                        width: 150,
                                        marginLeft: 25,
                                        alignSelf: 'center'
                                    }}
                                    source={{ uri: item.gambar }} />
                                <View style={styles.news}>
                                    <Text style={styles.newsTitle}>{item.name}</Text>

                                    <Text style={styles.newsValue}>{item.alamat}</Text>
                                </View>

                            </View>

                        </>

                    )
                }}
            />
        </View>


        // <SafeAreaView>

        //     <ScrollView>

        //         <View style={styles.container}>
        //             <View style={styles.banner}>
        //                 <ImageBackground
        //                     style={styles.ImageBackground} source={require('../../asset/banner.jpg')}
        //                 >
        //                     <Text style={styles.textimageback}>Selamat Datang</Text>
        //                     <Text style={styles.textimageback}>Perhimpunan Remaja Masjid </Text>
        //                     <Text style={styles.textimageback}>Dewan Masjid Indonesia </Text>
        //                     <Text style={styles.textimageback}>Jakarta Utara </Text>

        //                 </ImageBackground>
        //                 {/* <ImageBackground style={styles.ImageBackground} source={require('../../asset/banner.png')} /> */}

        //             </View>
        //             <Text style={styles.juduldalam}>Info Prima DMI JAKARTA UTARA</Text>
        //             <Button title="menuju halaman tambah" onPress={() => navigation.navigate("AddInfomasjid")} />
        //             <View style={styles.kotak}>
        //                 <View style={styles.kotakdalam}>
        //                     <Image
        //                         style={{
        //                             height: 100,
        //                             width: 150,
        //                             marginLeft: 25,
        //                             alignSelf: 'center'
        //                         }}
        //                         source={require('../../asset/masjid1.png')} />
        //                     <View style={styles.textnama}>
        //                         <Text style={styles.textcontent}>Masjid Islamic Center</Text>
        //                         <Text style={styles.textcontent}> Jl. Kramat Jaya Raya,
        //                         RW.1, Tugu Utara, Kec. Koja,
        //                         Kota Jkt Utara,
        //             Daerah Khusus Ibukota Jakarta 14260</Text>
        //                     </View>



        //                 </View>
        //                 <View style={styles.kotakdalam}>
        //                     <Image
        //                         style={{
        //                             height: 100,
        //                             width: 150,
        //                             marginLeft: 25,
        //                             alignSelf: 'center'
        //                         }}
        //                         source={require('../../asset/masjid1.png')} />
        //                     <View style={styles.textnama}>
        //                         <Text style={styles.textcontent}>Masjid Islamic Center</Text>
        //                         <Text style={styles.textcontent}> Jl. Kramat Jaya Raya,
        //                         RW.1, Tugu Utara, Kec. Koja,
        //                         Kota Jkt Utara,
        //             Daerah Khusus Ibukota Jakarta 14260</Text>
        //                     </View>
        //                 </View>
        //                 <View style={styles.kotakdalam}>
        //                     <Image
        //                         style={{
        //                             height: 100,
        //                             width: 150,
        //                             marginLeft: 25,
        //                             alignSelf: 'center'
        //                         }}
        //                         source={require('../../asset/masjid1.png')} />
        //                     <View style={styles.textnama}>
        //                         <Text style={styles.textcontent}>Masjid Islamic Center</Text>
        //                         <Text style={styles.textcontent}> Jl. Kramat Jaya Raya,
        //                         RW.1, Tugu Utara, Kec. Koja,
        //                         Kota Jkt Utara,
        //             Daerah Khusus Ibukota Jakarta 14260</Text>
        //                     </View>
        //                 </View>
        //                 <View style={styles.kotakdalam}>
        //                     <Image
        //                         style={{
        //                             height: 100,
        //                             width: 150,
        //                             marginLeft: 25,
        //                             alignSelf: 'center'
        //                         }}
        //                         source={require('../../asset/masjid1.png')} />
        //                     <View style={styles.textnama}>
        //                         <Text style={styles.textcontent}>Masjid Islamic Center</Text>
        //                         <Text style={styles.textcontent}> Jl. Kramat Jaya Raya,
        //                         RW.1, Tugu Utara, Kec. Koja,
        //                         Kota Jkt Utara,
        //             Daerah Khusus Ibukota Jakarta 14260</Text>
        //                     </View>
        //                 </View>


        //             </View>
        //         </View>
        //     </ScrollView>
        // </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 30
    },
    kotakButton: {
        padding: 15
    },
    kotakButton2: {
        padding: 15
    },
    TextTitle: {
        textAlign: 'center',
        marginBottom: 20
    },
    Line: {
        height: 2, backgroundColor: 'black', marginVertical: 20
    },
    avatar: {
        width: 100, height: 100, borderRadius: 100
    },
    input: {
        borderWidth: 1, marginBottom: 12,
        borderRadius: 25, paddingHorizontal: 12
    },
    itemContainer: {
        flexDirection: 'row', marginBottom: 20
    },
    news: {
        marginLeft: 18, flex: 1
    },
    newsTitle: {
        fontSize: 20,
        fontWeight: 'bold'
    },
    newsValue: {
        fontSize: 16
    },



})
