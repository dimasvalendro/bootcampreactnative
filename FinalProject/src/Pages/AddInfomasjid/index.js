import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Alert } from 'react-native'
import { TextInput } from 'react-native-gesture-handler'
import * as firebase from 'firebase'

export default function index({ navigation }) {
    const [name, setName] = useState("");
    const [alamat, setAlamat] = useState("");
    const [gambar, setGambar] = useState("");
    const submit = () => {
        const data = {
            name,
            alamat,
            gambar
        }
        if (name == '' || alamat == '') {
            Alert.alert('Error', 'Form tidak boleh kosong')
        } else {
            const namaTabel = 'infomasjid'
            firebase.database().ref('/' + namaTabel)
                .push(data)
                .then(() => {
                    setName("");
                    setAlamat("");
                    setGambar("");
                    Alert.alert('Sukses', 'Simpan Data berhasil')
                    navigation.navigate("Masjid")
                })
                .catch((err) => {
                    Alert.alert('Error', err.message)
                })
        }
    }
    return (
        <View style={styles.container}>
            <Text style={styles.text}>Input Info Masjid</Text>
            <View>
                <View style={styles.FormInput}>
                    <Text style={styles.FormText}>Nama Masjid</Text>
                    <TextInput
                        style={styles.Input}
                        placeholder="Masukin Nama Masjid"
                        value={name}
                        onChangeText={(value) => setName(value)}
                    />
                </View>
                <View style={styles.FormInput}>
                    <Text style={styles.FormText}>Alamat</Text>
                    <TextInput
                        value={alamat}
                        onChangeText={(value) => setAlamat(value)}
                        placeholder="Masukin Alamat"
                        style={styles.Input} />
                </View>
                <View style={styles.FormInput}>
                    <Text style={styles.FormText}>Gambar Url</Text>
                    <TextInput
                        value={gambar}
                        onChangeText={(value) => setGambar(value)}
                        placeholder="Masukin GambarUrl"
                        style={styles.Input} />
                </View>
                <View style={styles.kotakregister}>
                    <TouchableOpacity style={styles.ButtonRegister}
                        onPress={submit}
                    >
                        <Text style={styles.TextButton}>Simpan</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>

    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    text: {
        fontSize: 20,
        textAlign: 'center',
        padding: 20
    },
    FormInput: {
        marginHorizontal: 50,
        marginVertical: 6,
        alignContent: 'center',
        width: 294,
        margin: 25,
    },
    Input: {
        height: 50,
        borderColor: '#000000',
        borderWidth: 2,
        borderRadius: 10,

    },
    FormText: {
        color: '#000000',
        fontWeight: 'bold'

    },
    ButtonRegister: {
        alignItems: 'center',
        backgroundColor: '#000000',
        padding: 7,
        borderRadius: 10,
        marginHorizontal: 25,
        marginVertical: 10,
        width: 140,
        height: 40

    },
    kotakregister: {
        alignItems: 'center',

    },
    TextButton: {
        color: 'white'
    }
})
