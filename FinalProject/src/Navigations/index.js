import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createDrawerNavigator } from '@react-navigation/drawer'
import { NavigationContainer } from '@react-navigation/native'
import { FontAwesome5 } from '@expo/vector-icons';

import Home from '../Pages/Home'
import Login from '../Pages/Login'
import Anggota from '../Pages/Anggota'
import About from '../Pages/About'
import Masjid from '../Pages/Masjid'
import AddInfomasjid from '../Pages/AddInfomasjid'
import Register from '../Pages/Register'

const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

export default function Navigations() {
    return (
        <NavigationContainer>
            <Stack.Navigator
                screenOptions={{
                    headerShown: false
                }}
            >
                <Stack.Screen name="Login" component={Login} />
                <Stack.Screen name="Register" component={Register} />
                <Stack.Screen name="AddInfomasjid" component={AddInfomasjid} />
                <Stack.Screen name="Home" component={Home} />
                <Stack.Screen name="MainApp" component={MainApp} />
                <Stack.Screen name="MyDrawer" component={MyDrawer} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}
const MainApp = () =>
(
    <Tab.Navigator
        screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
                let iconName;

                if (route.name === 'Home') {
                    iconName = focused
                        ? 'home'
                        : 'home';
                    color = focused
                        ? 'green'
                        : 'green';
                } else if (route.name === 'Anggota') {
                    iconName = focused ? 'user-circle' : 'user-circle';
                    color = focused ? 'green' : 'green';
                } else if (route.name === 'Masjid') {
                    iconName = focused ? 'mosque' : 'mosque';
                    color = focused ? 'green' : 'green';
                }

                // You can return any component that you like here!
                return <FontAwesome5 name={iconName} size={size} color={color} />;
            },
        })}
    >
        <Tab.Screen name="Home" component={Home} />
        <Tab.Screen name="Anggota" component={Anggota} />
        <Tab.Screen name="Masjid" component={Masjid} />

    </Tab.Navigator>
)

const MyDrawer = () => (

    <Drawer.Navigator>
        <Drawer.Screen name="Beranda" component={MainApp} />
        <Drawer.Screen name="About" component={About} />
        <Drawer.Screen name="Masjid" component={Masjid} />
    </Drawer.Navigator>
)


const styles = StyleSheet.create({})
